
/*
 * Test the interpolation C code. Should work the same as INTERPOL function in IDL
 */


#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <iostream>

extern "C" {
    
#include "cpu_vertical_line_cgsolve.h"
#include "cpu_vertical_line.h"
}

const int INTERPOL_TEST_MAX_ARRAY_LENGTH = 100*512;

void test_vertical_line_interpol() {
	for (int nv=2; nv < 100; nv++) {
		for (int nu=2; nu < nv; nu++) {
			float u[INTERPOL_TEST_MAX_ARRAY_LENGTH]; 
			float v[INTERPOL_TEST_MAX_ARRAY_LENGTH];
			float x[INTERPOL_TEST_MAX_ARRAY_LENGTH];
			float vout[INTERPOL_TEST_MAX_ARRAY_LENGTH]; 
			for (int i=0; i < nv; i++) x[i] = i+0.1;
			for (int i=0; i < nv; i++) v[i] = ((float) x[i])*((float) x[i]);
			for (int i=0; i < nu; i++) u[i] = (nv-nu)/2 + i+0.6;
			cpu_vertical_line_interpol(v, nv, x, u, nu, vout);
			for (int i=0; i < nu; i++) assert(fabs(((float) vout[i] - u[i]*u[i])) < 0.5);
		}
	}
}

int main(void) {
	test_vertical_line_interpol();
}
