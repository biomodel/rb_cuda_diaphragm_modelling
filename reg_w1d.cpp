
/*

    1 dimensional image registration with wave equation constraints.
 
    Written in a very similar style to nifyreg but without spline interoplation.
 
    Author: Robert Barnett rbar9508@uni.sydney.edu.au
 
    Revision history:
 
        Mar 2013 Imported original code from 2011
        8 Mar 2013 Created a gradient descent version with arbirary step size 
        11 Mar 2013 Added directional constraint on compressibility
        Mar 2013 Implemented a linear conjugate gradient descent solver (cpu_vertical_line_cgsolve.c)
        19 Mar 2013 Implemented a multilevel imlementation. Added mass-preserving registration
        20 Mar 2013 Added compatibility for dynamic target images
        21 Mar 2013 Implemented non-linear conjugate gradient descent (cpu_vertical_line_nlcgsolve.c)
        22 Mar 2013 Added compatibility for solving for all times simultaneously (cpu_vertical_line_nlcgsolve_time.c)
        22 Mar 2013 Added wave equation constraint
 
 */

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include "_reg_tools.h"

extern "C" {
#include "nifti1_io.h"
#include "cpu_vertical_line.h"
#include "cpu_vertical_line_cgsolve.h"
#include "cpu_vertical_line_nlcgsolve.h"
#include "cpu_vertical_line_nlcgsolve_time.h"
}


typedef struct{
	char *targetImageName;
	char *sourceImageName;
	char *outputResultName;
	char *outputCPPName;
	int nIterValue;
    int numberOfLevelsValue;
    int numberOfLevelsToPerformValue;
    int numberOfTimePointsValue;
	float zCompressValue;
	float alphaValue;
}PARAM;
typedef struct{
	bool targetImageFlag;
	bool sourceImageFlag;
	bool outputResultFlag;
	bool nIterFlag;
	bool zCompressFlag;
	bool alphaFlag;
	bool outputCPPFlag;
    bool numberOfLevelsFlag;
    bool numberOfLevelsToPerformFlag;
    bool numberOfTimePointsFlag;
    bool solveAllTimeSimultaneouslyFlag;
    bool densityCorrectionFlag;
    bool averageTargetFlag;
}FLAG;


void PetitUsage(char *exec)
{
	fprintf(stderr,"1D Wave Equation algorithm for non-rigid registration in the x-direction only.\n");
	fprintf(stderr,"Usage:\t%s -target <targetImageName> -source <sourceImageName> [OPTIONS].\n",exec);
	fprintf(stderr,"\tSee the help for more details (-h).\n");
	return;
}

void Usage(char *exec)
{
	printf("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n");
}

int main(int argc, char **argv) {

	PARAM *param = (PARAM *)calloc(1,sizeof(PARAM));
	FLAG *flag = (FLAG *)calloc(1,sizeof(FLAG));
	
	/* read the input parameter */
	for(int i=1;i<argc;i++){
		if(strcmp(argv[i], "-help")==0 || strcmp(argv[i], "-Help")==0 ||
		   strcmp(argv[i], "-HELP")==0 || strcmp(argv[i], "-h")==0 ||
		   strcmp(argv[i], "--h")==0 || strcmp(argv[i], "--help")==0){
			Usage(argv[0]);
			return 0;
		}
		else if(strcmp(argv[i], "-target") == 0){
			param->targetImageName=argv[++i];
			flag->targetImageFlag=1;
		}
		else if(strcmp(argv[i], "-source") == 0){
			param->sourceImageName=argv[++i];
			flag->sourceImageFlag=1;
		}
		else if(strcmp(argv[i], "-cpp") == 0){
			param->outputCPPName=argv[++i];
			flag->outputCPPFlag=1;
		}
		else if(strcmp(argv[i], "-result") == 0){
			param->outputResultName=argv[++i];
			flag->outputResultFlag=1;
		}
		else if(strcmp(argv[i], "-nIter") == 0){
			param->nIterValue=atoi(argv[++i]);
			flag->nIterFlag=1;
		}
		else if(strcmp(argv[i], "-zCompress") == 0){
			param->zCompressValue=(float) (atof(argv[++i]));
			flag->zCompressFlag=1;
		}
		else if(strcmp(argv[i], "-alpha") == 0){
			param->alphaValue=(float)fabs((atof(argv[++i])));
			flag->alphaFlag=1;
		}
		else if(strcmp(argv[i], "-nl") == 0){
			param->numberOfLevelsValue=(int) (atoi(argv[++i]));
			flag->numberOfLevelsFlag=1;
		}
		else if(strcmp(argv[i], "-np") == 0){
			param->numberOfLevelsToPerformValue=(int) (atoi(argv[++i]));
			flag->numberOfLevelsToPerformFlag=1;
		}
    	else if(strcmp(argv[i], "-nt") == 0){
			param->numberOfTimePointsValue=(int) (atoi(argv[++i]));
			flag->numberOfTimePointsFlag=1;
		}
    	else if(strcmp(argv[i], "-allTimes") == 0){
			flag->solveAllTimeSimultaneouslyFlag=1;
        }
        else if(strcmp(argv[i], "-density") == 0){
                flag->densityCorrectionFlag=1;
        }
        else if(strcmp(argv[i], "-average") == 0){
                flag->averageTargetFlag=1;
        } else{
			fprintf(stderr,"Err:\tParameter %s unknown.\n",argv[i]);
			PetitUsage(argv[0]);
			return 1;
		}
        
        
	}
	
	
	if(!flag->targetImageFlag || !flag->sourceImageFlag){
		fprintf(stderr,"Err:\tThe target and the source image have to be defined.\n");
		PetitUsage(argv[0]);
		return 1;
	}	

	if (! flag->nIterFlag) param->nIterValue=500;
	if (! flag->zCompressFlag) param->zCompressValue=0;
	if (! flag->alphaFlag) param->alphaValue=0;
    if (! flag->numberOfLevelsFlag) param->numberOfLevelsValue=3;
    if (! flag->numberOfLevelsToPerformFlag) param->numberOfLevelsToPerformValue=param->numberOfLevelsValue;
	nifti_image *targetHeader;
	nifti_image *sourceHeader;
	
	nifti_image *targetImage = nifti_image_read(param->targetImageName,true);
	if(targetImage == NULL){
		fprintf(stderr,"* ERROR Error when reading the target image: %s\n",param->targetImageName);
		return 1;
	}
	reg_changeDatatype<float>(targetImage);
	nifti_image *sourceImage = nifti_image_read(param->sourceImageName,true);
	if(sourceImage == NULL){
		fprintf(stderr,"* ERROR Error when reading the source image: %s\n",param->sourceImageName);
		return 1;
	}
	reg_changeDatatype<float>(sourceImage);

	printf("Target image size: \t%ix%ix%i voxels\t%gx%gx%g mm\n",
		   targetImage->nx, targetImage->ny, targetImage->nz, targetImage->dx, targetImage->dy, targetImage->dz);
	printf("Source image size: \t%ix%ix%i voxels\t%gx%gx%g mm\n",
		   sourceImage->nx, sourceImage->ny, sourceImage->nz, sourceImage->dx, sourceImage->dy, sourceImage->dz);

	printf("Alpha is %f . zCompress is %f \n",
		   param->alphaValue, param->zCompressValue);

    printf("Number of time points in target image %i\n",targetImage->nt);

    if (! flag->numberOfTimePointsFlag) param -> numberOfTimePointsValue = targetImage->nt;

    assert(param -> numberOfTimePointsValue <= targetImage->nt);

    if (flag -> solveAllTimeSimultaneouslyFlag) {
        printf("Solving all time points simultaneously\n");
    }
    
    const int nt = param -> numberOfTimePointsValue;
    printf("Processing the first %i time points\n",nt);


	int dim_cpp[8];
	dim_cpp[0]=4;
	dim_cpp[1]=targetImage->nx;
	dim_cpp[2]=targetImage->ny;
	dim_cpp[3]=targetImage->nz;
	dim_cpp[4]=nt;
    dim_cpp[5]=dim_cpp[6]=dim_cpp[7]=1;
	nifti_image *controlPointImage = nifti_make_new_nim(dim_cpp, NIFTI_TYPE_FLOAT32, true);
	
	/* allocate the result image */
	nifti_image *resultImage = nifti_copy_nim_info(targetImage);
	resultImage->datatype = sourceImage->datatype;
	resultImage->nbyper = sourceImage->nbyper;
	resultImage->data = (void *)calloc(resultImage->nvox, resultImage->nbyper);
	
	// Cannot handle different resolution data (yet)
	assert(targetImage->nz == sourceImage->nz);
	assert(targetImage->ny == sourceImage->ny);
	assert(targetImage->nx == sourceImage->nx);

    int nIter =0;

    
#ifndef NDEBUG
    // Determine the maximum squared difference which is possible for the image. This is necessary for scaling the gradient in the solver
    float min_target=0.;
    float min_source=0.;
    float max_target=0.;
    float max_source=0.;
    for (long voxOffset = 0 ; voxOffset < targetImage->nz*targetImage->ny*targetImage->nx; voxOffset++) {
        min_target = fmin(min_target,((float*) (targetImage->data))[voxOffset]);
        max_target = fmax(max_target,((float*) (targetImage->data))[voxOffset]);
        min_source = fmin(min_source,((float*) (sourceImage->data))[voxOffset]);
        max_source = fmax(max_source,((float*) (sourceImage->data))[voxOffset]);
    }
    float max_grad = fmax((max_target-min_source)*(max_target-min_source),(max_source-min_target)*(max_source-min_target));
    printf("[DEBUG] Max squared difference: %f\n", max_grad);
#endif

    
    for (int currentLevel=0; currentLevel < param->numberOfLevelsToPerformValue; currentLevel++) {
        nIter=0;
        int dline = pow(2,param->numberOfLevelsValue-currentLevel-1);
        long ndz = (targetImage->nz)/dline;
        long ndy = (targetImage->ny)/dline;
    // Iterate over each y,z position and determine the pointer to a line of voxels in the x direction
        printf("Performing level %i with spacing %i\n", currentLevel, dline);

        // Determine the maximum squared difference which is possible for the image. This is necessary for scaling the gradient in the solver
        float min_target=0.;
        float min_source=0.;
        float max_target=0.;
        float max_source=0.;
        for (long dz = 0 ; dz < ndz; dz++) {
            for (long dy = 0 ; dy < ndy; dy++) {
                for (long x = 0; x < targetImage->nx; x++) {
                    float sourceVoxel=0;
                    float targetVoxel=0;
                    int z = dz*dline;
                    int y = dy*dline;
                    long dlinez = std::min(dline,targetImage->nz-z);
                    long dliney = std::min(dline,targetImage->ny-y);
                    float ndline = dlinez*dliney;
                    for (long iz = 0 ; iz < dlinez; iz++) {
                        for (long iy = 0 ; iy < dliney; iy++) {
                            z = dz*dline+iz;
                            y = dy*dline+iy;
                            long voxOffset = (z*targetImage->ny + y)*targetImage->nx + x;
                            sourceVoxel += (((float*) sourceImage->data)[voxOffset])/ndline;
                            for (long t = 0; t < nt; t++) {
                                voxOffset = ((t*targetImage->nz + z)*targetImage->ny + y)*targetImage->nx + x;
                                targetVoxel += (((float*) targetImage->data)[voxOffset])/ndline/nt;
                            }
                        }
                    }
                    min_target = fmin(min_target,targetVoxel);
                    max_target = fmax(max_target,targetVoxel);
                    min_source = fmin(min_source,sourceVoxel);
                    max_source = fmax(max_source,sourceVoxel);
                }
            }
        }
    
        float max_grad = fmax((max_target-min_source)*(max_target-min_source),(max_source-min_target)*(max_source-min_target));
#ifndef NDEBUG
        printf("[DEBUG] Max squared difference at level %i: %f\n", currentLevel, max_grad);
#endif
        
        #pragma omp parallel private(dz)
        for (long dz = 0 ; dz < ndz; dz++) {
            for (long dy = 0 ; dy < ndy; dy++) {
                int z = dz*dline;
                int y = dy*dline;
                float* l_result = new float[targetImage->nx*nt];
                float* l_target = new float[targetImage->nx*nt];
                float* l_source = new float[targetImage->nx];
                float* l_control = new float[targetImage->nx*nt];
                float* l_u = new float[targetImage->nx*nt];
                long dlinez = std::min(dline,targetImage->nz-z);
                long dliney = std::min(dline,targetImage->ny-y);
                assert(dlinez > 0);
                assert(dliney > 0);
                float ndline = dlinez*dliney;
                for (int x = 0; x < targetImage->nx; x++) {
                    l_source[x] = 0;
                    for (int t = 0; t < nt; t++) {
                        l_u[t*targetImage->nx + x] = 0;
                        l_control[t*targetImage->nx + x] = 0;
                        l_result[t*targetImage->nx + x] = 0;
                        l_target[t*targetImage->nx + x] = 0;
                    }
                }
                for (long iz = 0 ; iz < dlinez; iz++) {
                    for (long iy = 0 ; iy < dliney; iy++) {
                        z = dz*dline+iz;
                        y = dy*dline+iy;
                        assert(z >= 0);
                        assert(y >= 0);
                        assert(z < targetImage -> nz);
                        assert(y < targetImage -> ny);
                        // Determine the voxel offset of the line.
                        for (long x = 0; x < targetImage->nx; x++) {
                            long voxOffset = (z*targetImage->ny + y)*targetImage->nx + x;
                            assert((voxOffset) < sourceImage->nvox);
                            assert((voxOffset) >= 0);
                            l_source[x] += ((float*) sourceImage->data)[voxOffset]/ndline;
                            for (long t = 0; t < nt; t++) {
                                voxOffset = ((t*targetImage->nz + z)*targetImage->ny + y)*targetImage->nx + x;
                                assert((voxOffset) < targetImage->nvox);
                                assert((voxOffset) >= 0);
                                l_target[t*targetImage->nx + x] += ((float*) targetImage->data)[voxOffset]/ndline;
                                if (currentLevel > 0) 
                                    l_control[t*targetImage->nx + x] += ((float*) controlPointImage->data)[voxOffset]/ndline;
                                else
                                    l_control[t*targetImage->nx + x] = x;
                            }
                        }
                    }
                }
#ifndef NDEBUG
                if (dz == ndz/2) {
                    if (dy == ndy/2) {
                        printf("[DEBUG] Target %f\n", l_target[targetImage->nx/2]);
                        printf("[DEBUG] Source %f\n", l_source[targetImage->nx/2]);
                    }
                }
#endif

                // Prepare l_u
                for (long t = 0; t < nt; t++) {
                    for (long x = 0; x < targetImage->nx; x++) {
                        l_u[t*targetImage->nx + x] = l_control[t*targetImage->nx + x] - x;
                    }
                }
 
                
                if (flag -> solveAllTimeSimultaneouslyFlag) {
                    // Solve all time points at each line using a conjugate gradient solver
                    if ( flag->averageTargetFlag ) {
                        // Multiframe floating image registered to an average target image
                        nIter += cpu_vertical_line_nlcgsolve_time_cpp(l_target, l_source, l_result, l_control, l_u, targetImage->nx,
                                                                      nt, param->nIterValue, param->zCompressValue, flag->zCompressFlag == 1,
                                                                      param->alphaValue, max_grad, currentLevel == 0, 1, flag->densityCorrectionFlag, 1);
                    } else {
                        nIter += cpu_vertical_line_nlcgsolve_time_cpp( l_source, l_target, l_result, l_control, l_u, targetImage->nx,
                                                                      nt, param->nIterValue, param->zCompressValue, flag->zCompressFlag == 1,
                                                                      param->alphaValue, max_grad, currentLevel == 0, 0, flag->densityCorrectionFlag, 0);
                    }
                        // Solve all time points at each line using a conjugate gradient solver
    /*
     nIter += cpu_vertical_line_nlcgsolve_time_cpp( l_target, l_source, l_result, l_control, l_u, targetImage->nx,
                                                                  nt, param->nIterValue, param->zCompressValue, flag->zCompressFlag == 1,
                                                                  param->alphaValue, max_grad, currentLevel == 0, 1, density_corr);
     */
                } else {
                    for (long ti = 0; ti < nt; ti++) {
                        // Solve each line individually using a conjugate gradient solver
 
                        // It shouldn't matter which way they are solved. Both of these should produce the same result
 /*
                        int nBackward = cpu_vertical_line_nlcgsolve_cpp( l_target, l_source, l_result, l_control, l_u, targetImage->nx, ti, nt,
                                                                 param->nIterValue, param->zCompressValue, flag->zCompressFlag == 1,
                                                                 param->alphaValue, max_grad, currentLevel == 0, 1, density_corr);
*/
                        int nForward = cpu_vertical_line_nlcgsolve_cpp( l_source, l_target, l_result, l_control, l_u, targetImage->nx, ti, nt,
                                                                 param->nIterValue, param->zCompressValue, flag->zCompressFlag == 1,
                                                                 param->alphaValue, max_grad, currentLevel == 0, 0, flag->densityCorrectionFlag);
 
#ifndef NDEBUG
              //          printf("[DEBUG] ti=%ld, nForward=%i, nBackward=%i\n",ti,  nForward, nBackward);
#endif
                    }
                }
                
                for (long iz = 0 ; iz < dlinez; iz++) {
                    for (long iy = 0 ; iy < dliney; iy++) {
                        z = dz*dline+iz;
                        y = dy*dline+iy;
                        assert(z >= 0);
                        assert(y >= 0);
                        assert(z < targetImage -> nz);
                        assert(y < targetImage -> ny);
                        for (long x = 0; x < targetImage->nx; x++) {
                            long voxOffset = (z*targetImage->ny + y)*targetImage->nx + x;
                            assert((voxOffset) < sourceImage->nvox);
                            assert((voxOffset) >= 0);
                            l_source[x] += ((float*) sourceImage->data)[voxOffset]/ndline;
                            for (long t = 0; t < nt; t++) {
                                voxOffset = ((t*targetImage->nz + z)*targetImage->ny + y)*targetImage->nx + x;
                                assert((voxOffset) < controlPointImage->nvox);
                                assert((voxOffset) >= 0);
                                assert((voxOffset) < resultImage->nvox);
                                assert((voxOffset) >= 0);
                                ((float*) resultImage->data)[voxOffset] = l_result[t*targetImage->nx + x];
                                ((float*) controlPointImage->data)[voxOffset] = l_control[t*targetImage->nx + x];
                            }
                        }
                    }
                }
        
                delete l_result;
                delete l_target;
                delete l_source;
                delete l_control;
                delete l_u;

            }
        }
        printf("Total number of iterations: %i in %ld lines\n", nIter, ndz*ndy);
    }
    
    if(!flag->outputCPPFlag) param->outputCPPName=(char *)"outputCPP.nii";
	/* The best result is returned */
	nifti_set_filenames(controlPointImage, param->outputCPPName, 0, 0);
	nifti_image_write(controlPointImage);
	nifti_image_free(controlPointImage);
	
	if(!flag->outputResultFlag) param->outputResultName=(char *)"outputResult.nii";
	nifti_set_filenames(resultImage, param->outputResultName, 0, 0);
	nifti_image_write(resultImage);
	nifti_image_free(resultImage);
}


