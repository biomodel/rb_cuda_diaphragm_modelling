

#include "cpu_vertical_line.h"
#include "cpu_vertical_line_cgsolve.h"
#include "cpu_vertical_line_nlcgsolve.h"
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <omp.h>


const int CG_MAX_ARRAY_LENGTH = 100*512;


void cpu_vertical_line_density_corr( float* l_u, float* l_reg, int nl, int density_corr) {
    int hi;
    float l_gradient[CG_MAX_ARRAY_LENGTH];
    
    cpu_vertical_line_deriv5(l_u, nl, l_gradient );
    
    // Mass preserving image registration.
    float zdens;
    for ( hi=0; hi< nl; hi++) {
        zdens = 1.0;
        if (density_corr > 0) zdens = fabs(l_gradient[hi] + 1.0);
        assert(zdens < 1e10);
        l_reg[hi] = l_reg[hi]*zdens;
    }

}

// Align the floating image to the target.
// Control points are updated and overwritten

void cpu_vertical_line_warp( float* t_float, float* t_u, float* t_control, float* t_reg, int nl, int ti, int nt, int multiframe_float, int density_corr) {
    float* l_float;
    
    if (multiframe_float > 0) {
        l_float = t_float + nl*ti;
    } else {
        l_float = t_float;
    }
    
    float* l_control = t_control + ti*nl;
    float* l_reg = t_reg + ti*nl;
    float* l_u = t_u + ti*nl;
    
    int hi;
	float l_inds[CG_MAX_ARRAY_LENGTH];
	float l_float_tmp[CG_MAX_ARRAY_LENGTH];
	for ( hi=0; hi< nl; hi++) l_inds[hi] = hi;
    
    for (hi=0 ; hi < nl; hi++) {
        assert(isfinite(l_u[hi]));
        if ((hi + l_u[hi]) < 0) l_u[hi]=-hi;
        if ((hi + l_u[hi]) > (nl-1)) l_u[hi]=(nl-1)-hi;
        assert(l_control[hi] >= 0);
        assert(l_control[hi] < nl);
        l_control[hi] = hi + l_u[hi];
        assert(l_control[hi] >= 0);
        assert(l_control[hi] < nl);
    }
    
    for (hi=0 ; hi < nl; hi++) l_float_tmp[hi] = l_float[hi];
    
    // It doesn't seem correct to attmpt density correction before transofrmation
//    if (density_corr > 0) cpu_vertical_line_density_corr( l_u, l_float_tmp, nl, density_corr);

    cpu_vertical_line_interpol(l_float_tmp, nl, l_inds, l_control, nl, l_reg);
    
    if (density_corr > 0) cpu_vertical_line_density_corr( l_u, l_reg, nl, density_corr);
}

/*
float cpu_vertical_line_compute_energy_invertible( float* t_float, float* t_target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float max_grad, int multiframe_float) {
    float totalEnergy = 0;
    float* u_inv = u + nl*nt;
    totalEnergy += cpu_vertical_line_compute_energy( t_float, t_target, u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float);
    totalEnergy += cpu_vertical_line_compute_energy( t_target, t_float, u_inv, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, ! multiframe_float);
    // Create a energy term for non invertibility 
    return totalEnergy;
}
*/

float cpu_vertical_line_compute_average( float* t_float, float* l_average, float* t_u, int nl, int nt, int density_corr )  {
    
    int av_t = 0;
    int hi = 0;
    
    float l_inds[CG_MAX_ARRAY_LENGTH];
    for ( hi=0; hi< nl; hi++) l_inds[hi] = hi;
    float l_control[CG_MAX_ARRAY_LENGTH];
    float l_reg2[CG_MAX_ARRAY_LENGTH];

    for (hi=0 ; hi < nl; hi++) l_average[hi] = 0;

    
    // Create the average target
    for (av_t = 0; av_t < nt; av_t++) {
        float* l_u = t_u + av_t*nl;
        float* l_float = t_float + av_t*nl;
        for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] + 0;
        cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg2);
        // Apply density correction when claculating energy
        if (density_corr > 0) cpu_vertical_line_density_corr( l_u, l_reg2, nl, density_corr);
        // Add to the average
        for (hi=0 ; hi < nl; hi++) l_average[hi] += l_float[hi]/nt;
    }

    float totalCounts = 0;
    for (hi=0 ; hi < nl; hi++) totalCounts += l_average[hi];
    return(totalCounts);
}

/*
float cpu_vertical_line_compute_variance_energy( float* t_float, float* l_average, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float max_grad, int multiframe_float, int density_corr ) {
    if (useZCompress) assert(nt > 2); // need at least 3 points for a time derivative
    assert(nl > 2); // need at least 3 points for a space derivative
    assert(multiframe_float > 0); // This must be a multiframe float

    int av_t = 0;
    int hi = 0;   
    float l_inds[CG_MAX_ARRAY_LENGTH];
    for ( hi=0; hi< nl; hi++) l_inds[hi] = hi;
    float l_control[CG_MAX_ARRAY_LENGTH];
    float l_reg2[CG_MAX_ARRAY_LENGTH];

    float totalEnergy = 0;
    float energy_line2;
    // Calculate the variance for each time point
    for (av_t = 0; av_t < nt; av_t++) {
        float* l_u = u + av_t*nl;
        float* l_float = t_float + av_t*nl;
        for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] + 0;
        cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg2);
        // Apply density correction when claculating energy
        if (density_corr > 0) cpu_vertical_line_density_corr( l_u, l_reg2, nl, density_corr);
        for (hi=0 ; hi < nl; hi++) {
            if ((hi > 0) & (hi < (nl-1))) {
                energy_line2 = (l_reg2[hi]-l_average[hi])*(l_reg2[hi]-l_average[hi]);
                totalEnergy += energy_line2*(1.0-alpha)/max_grad;
            }
        }
    }
    
    // Compute the laplacian at this time point only
    float* l_u = u + ti*nl;
    float zlaplacian1, zgradient;
    for (hi=0 ; hi < nl; hi++) {
        if ((hi > 0) & (hi < (nl-1))) {
            // Compute the first derivative of the line transform hi => hi + l_u[hi]
            //            zgradient = (l_u[hi+1] + (h+1) - (l_u[hi-1] + (h-1)))/2.0;
            zgradient = (2 + l_u[hi+1] - l_u[hi-1])/2.0;
            // Compute the second derivative of the displacement u w.r.t height
            zlaplacian1 = (l_u[hi+1] - 2*l_u[hi] + l_u[hi-1]);
            if (useZCompress > 0) {
                int tt = ti; // Just assume that laplacian is constant at end time frames
                if (tt < 1) tt = 1;
                if (tt > (nt-2)) tt = nt-2;
                assert((tt > 0) & (tt < (nt-1)));
                zlaplacian1 -= zCompress*(u[(tt+1)*nl + hi] - 2*u[tt*nl + hi] + u[(tt-1)*nl + hi]);
                
                // Apply local rigidity constraint (scaled by alpha excluding linear penalty)
                totalEnergy += (zlaplacian1*zlaplacian1)/2.0*alpha;
            } else {
                // Apply local rigidity constraint (scaled by alpha)
                totalEnergy += (zlaplacian1*zlaplacian1)/2.0*alpha;
            }
        }
    }

    assert(totalEnergy >= 0);
    return(totalEnergy);
}
*/

float cpu_vertical_line_compute_energy( float* t_float, float* t_target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float max_grad, int multiframe_float, int density_corr) {
    if (useZCompress) assert(nt > 2); // need at least 3 points for a time derivative
    assert(nl > 2); // need at least 3 points for a space derivative
    
	float totalEnergy = 0., totalCounts=0., energy_line2, zgradient, zdens;
    float zlaplacian1;
    int hi=0;
//    float nhi=0;
    float zdiff=0;
    float* l_float;
    float* l_target;
    if (multiframe_float > 0) {
        l_float = t_float + ti*nl;
        l_target = t_target;
    } else {
        l_float = t_float;
        // Multiframe target
        l_target = t_target + ti*nl;
        
    }

    float* l_u = u + ti*nl;

    for (hi=0 ; hi < nl; hi++) {
        totalCounts += fabs(l_target[hi]);
        totalCounts += fabs(l_float[hi]);
    }
    
    if (totalCounts < nl) {
        return(0);
    }
    
	float l_inds[CG_MAX_ARRAY_LENGTH];
	for ( hi=0; hi< nl; hi++) l_inds[hi] = hi;
	float l_control[CG_MAX_ARRAY_LENGTH];
    
	float l_reg2[CG_MAX_ARRAY_LENGTH];
    
    for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] + 0;
    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg2);
    
    // Apply density correction when claculating energy 
    if (density_corr > 0) cpu_vertical_line_density_corr( l_u, l_reg2, nl, density_corr);

    for (hi=0 ; hi < nl; hi++) {
    //    nhi = hi + l_u[hi];
   //     if ((nhi > 0) & (nhi < (nl-1))) {
            zdens = 1.0;
            if ((hi > 0) & (hi < (nl-1))) {
                // Compute the first derivative of the line transform hi => hi + l_u[hi]
                //            zgradient = (l_u[hi+1] + (h+1) - (l_u[hi-1] + (h-1)))/2.0;
                zgradient = (2 + l_u[hi+1] - l_u[hi-1])/2.0;
                // Compute the second derivative of the displacement u w.r.t height
                zlaplacian1 = (l_u[hi+1] - 2*l_u[hi] + l_u[hi-1]);
                if (useZCompress > 0) {
                    int tt = ti; // Just assume that laplacian is constant at end time frames
                    if (tt < 1) tt = 1;
                    if (tt > (nt-2)) tt = nt-2;
                    assert((tt > 0) & (tt < (nt-1)));
                    zlaplacian1 -= zCompress*(u[(tt+1)*nl + hi] - 2*u[tt*nl + hi] + u[(tt-1)*nl + hi]);

                    // Apply local rigidity constraint (scaled by alpha excluding linear penalty)
                    totalEnergy += (zlaplacian1*zlaplacian1)/2.0*alpha;
                } else {
                    // Apply local rigidity constraint (scaled by alpha)
                    totalEnergy += (zlaplacian1*zlaplacian1)/2.0*alpha;
                }
                // Trying to stop the problem of vansihing activity
                // The higher the gradient, the more the control points are spaced out. This means that we are missing points in the floating image. A correction term would increase activity
                // The lower the gradient, the more the control points are compressed.This means that we are oversampling points in the floating image. A correction term would decrease activity
                
                if (density_corr > 0) zdens = fabs(zgradient);
                assert(zdens < 1e10);
            }
            // Compute the squared difference to determine image similarity
            energy_line2 = (l_reg2[hi]-l_target[hi])*(l_reg2[hi]-l_target[hi]);
            totalEnergy += energy_line2*(1.0-alpha)/max_grad;
 //           printf("[DEBUG] energy [%i] [%i] Total energy: %f, zdens: %f, zlaplacian1: %f, energy_line2 %f\n",hi, ti, totalEnergy, zdens, zlaplacian1, energy_line2);
 //       }
    }
    assert(totalEnergy >= 0);
    return(totalEnergy);
}

/*
float cpu_vertical_line_compute_gradient_invertible( float* t_float, float* t_target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float* gradient, float max_grad, int multiframe_float) {
    
    float* gradient_inv = gradient + nl*nt;
    float* u_inv = u + nl*nt;
    float totalEnergy = 0;
    totalEnergy += cpu_vertical_line_compute_gradient( t_float, t_target, u,  nl,  ti,  nt,  zCompress,  useZCompress,  alpha, gradient,  max_grad, multiframe_float);
    totalEnergy += cpu_vertical_line_compute_gradient( t_target, t_float, u_inv,  nl,  ti,  nt,  zCompress,  useZCompress,  alpha, gradient_inv,  max_grad, ! multiframe_float);
    // Work out a gradient term for non invertibility
    
    return totalEnergy;
}
*/

// Compute the gradient for what control point displacements (u) match the float to the target.
// Alpha is a scaling factor for local compressibility. 0 mean no constraint, 1 means rigid
// zCompress is a scaling factor for linear compressibility. This can be used to favour compression (negative) or expansion (positive)
// Image similarity is calculated by miniising the difference between the transformed floating line and the target line.
float cpu_vertical_line_compute_gradient( float* t_float, float* t_target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float* gradient, float max_grad, int multiframe_float, int density_corr) {
    if (useZCompress) assert(nt > 2); // need at least 3 points for a time derivative
    assert(nl > 2); // need at least 3 points for a space derivative

	float totalEnergy = 0., totalCounts=0., energy_line0, energy_line1, energy_line2, energy_line3, energy_line4, zgradient, zdens;
    float zlaplacian0, zlaplacian1, zlaplacian2;
    int hi=0;
  //  float nhi=0;
    float zdiff=0;
    // l_float image is single frame
    float* l_float;
    float* l_target;
    if (multiframe_float > 0) {
        l_float = t_float + ti*nl;
        l_target = t_target;
    } else {
        l_float = t_float;
        // Multiframe target
        l_target = t_target + ti*nl;
        
    }
    float* l_u = u + ti*nl;
    float* l_gradient = gradient + ti*nl;
    
    for (hi=0 ; hi < nl; hi++) {
        totalCounts += fabs(l_target[hi]);
        totalCounts += fabs(l_float[hi]);
    }
    
    if (totalCounts < nl) {
        for (hi=0 ; hi < nl; hi++) {
            l_gradient[hi] = 0;
        }
        return(0);
    }
    
	float l_inds[CG_MAX_ARRAY_LENGTH];
	for ( hi=0; hi< nl; hi++) l_inds[hi] = hi;
	float l_control[CG_MAX_ARRAY_LENGTH];

    /*
    float l_reg0[CG_MAX_ARRAY_LENGTH];
	float l_reg1[CG_MAX_ARRAY_LENGTH];
	float l_reg3[CG_MAX_ARRAY_LENGTH];
	float l_reg4[CG_MAX_ARRAY_LENGTH];
    for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] - 1;
    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg0);
    for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] - 0.5;
    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg1);
    for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] + 0.5;
    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg3);
    for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi] + 1.0;
    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg4);
*/
    
    float l_reg2[CG_MAX_ARRAY_LENGTH];
    for (hi=0 ; hi < nl; hi++) l_control[hi] = hi + l_u[hi];
    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg2);

    float l_reg2_corr[CG_MAX_ARRAY_LENGTH];
    for (hi=0; hi < nl; hi++) l_reg2_corr[hi] = l_reg2[hi];
    // According to theielmens, desnity correction is not needed when finding direction (gradient), only magnitude
    if (density_corr > 0) cpu_vertical_line_density_corr( l_u, l_reg2_corr, nl, density_corr);
    
    float l_reg2_deriv[CG_MAX_ARRAY_LENGTH];
    cpu_vertical_line_deriv(l_reg2, nl, l_reg2_deriv);

    // Theielemens method
    float l_diff_deriv[CG_MAX_ARRAY_LENGTH];
    float l_diff[CG_MAX_ARRAY_LENGTH];
    for (hi=0 ; hi < nl; hi++) {
        l_diff[hi] = l_target[hi] - l_reg2[hi];
    }
    cpu_vertical_line_deriv(l_diff, nl, l_diff_deriv);
        
    for (hi=0 ; hi < nl; hi++) {
        l_gradient[hi] = 0;
 //       nhi = hi + l_u[hi];
 //       if ((nhi > 0) & (nhi < (nl-1))) {
            zdens = 1.0;
            if ((hi > 0) & (hi < (nl-1))) {
                // Compute the first derivative of the line transform hi => hi + l_u[hi]
                //            zgradient = (l_u[hi+1] + (h+1) - (l_u[hi-1] + (h-1)))/2.0;
                zgradient = (2 + l_u[hi+1] - l_u[hi-1])/2.0;
                // Compute the second derivative of the displacement u w.r.t height
                zlaplacian0 = (l_u[hi+1] - 2*(l_u[hi]-1) + l_u[hi-1]);
                zlaplacian1 = (l_u[hi+1] - 2*l_u[hi] + l_u[hi-1]);
                zlaplacian2 = (l_u[hi+1] - 2*(l_u[hi]+1) + l_u[hi-1]);
                if (useZCompress > 0) {
                    int tt = ti; // Just assume that laplacian is constant at end time frames
                    if (tt < 1) tt = 1;
                    if (tt > (nt-2)) tt = nt-2;
                    assert((tt > 0) & (tt < (nt-1)));
                    zlaplacian0 -= zCompress*(u[(tt+1)*nl + hi] - 2*(u[tt*nl + hi]-1) + u[(tt-1)*nl + hi]);
                    zlaplacian1 -= zCompress*(u[(tt+1)*nl + hi] - 2*u[tt*nl + hi] + u[(tt-1)*nl + hi]);
                    zlaplacian2 -= zCompress*(u[(tt+1)*nl + hi] - 2*(u[tt*nl + hi]+1) + u[(tt-1)*nl + hi]);

                    // Apply local rigidity constraint (scaled by alpha excluding linear penalty)
                    l_gradient[hi] += (zlaplacian2*zlaplacian2-zlaplacian0*zlaplacian0)/2.0*alpha;
                    totalEnergy += (zlaplacian1*zlaplacian1)/2.0*alpha;
                } else {
                    // Apply local rigidity constraint (scaled by alpha)
                    l_gradient[hi] += (zlaplacian2*zlaplacian2-zlaplacian0*zlaplacian0)/2.0*alpha;
                    totalEnergy += (zlaplacian1*zlaplacian1)/2.0*alpha;
                }
                // Trying to stop the problem of vansihing activity 
                // The higher the gradient, the more the control points are spaced out. This means that we are missing points in the floating image. A correction term would increase activity
                // The lower the gradient, the more the control points are compressed.This means that we are oversampling points in the floating image. A correction term would decrease activity
                
                if (density_corr > 0) zdens = fabs(zgradient);
                assert(zdens < 1e10);
              }
            
 // Compute the squared difference to determine image similarity
/*
 energy_line0 = (l_reg0[hi]-l_target[hi])*(l_reg0[hi]-l_target[hi]);
            energy_line1 = (l_reg1[hi]-l_target[hi])*(l_reg1[hi]-l_target[hi]);
            energy_line3 = (l_reg3[hi]-l_target[hi])*(l_reg3[hi]-l_target[hi]);
            energy_line4 = (l_reg4[hi]-l_target[hi])*(l_reg4[hi]-l_target[hi]);
 */
            energy_line2 = (l_reg2_corr[hi]-l_target[hi])*(l_reg2_corr[hi]-l_target[hi]);
            totalEnergy += energy_line2*(1.0-alpha)/max_grad; // Clearly something amis here
            
  //          printf("[DEBUG] gradient [%i] [%i] Total energy: %f, zdens: %f, zlaplacian1: %f, energy_line2 %f\n",hi, ti, totalEnergy, zdens, zlaplacian1, energy_line2);
            // Five point finite difference
            //l_gradient[hi] += (-energy_line4 + 8*energy_line3 - 8*energy_line1 + energy_line0)/12.0*(1.0-alpha)/max_grad;
   
            
//            l_gradient[hi] -= 2 * (l_target[hi] - l_reg2[hi]) * l_reg2_deriv[hi] * (1.0-alpha) /max_grad;
            // Theielemens method
            if (density_corr > 0) l_gradient[hi] += 2 * l_diff_deriv[hi] * l_reg2[hi] * (1.0-alpha) /max_grad;
            else l_gradient[hi] -= 2 * (l_target[hi] - l_reg2[hi]) * l_reg2_deriv[hi] * (1.0-alpha) /max_grad;
            //(-energy_line4 + 8*energy_line3 - 8*energy_line1 + energy_line0)/12.0*(1.0-alpha)/max_grad;
 ///       }
    }
    return(totalEnergy);
}

// Warp a floating curve into a target curve by minimising absolute difference in combination with 1D wave equation along the curve.
// stepSize is the step length
// nIter is the number of iterations
// l_float[nl]
// target[nl*nt]
// reg[nl*nt]
// control[nl*nt]
// u[nl*nt]
// u should remain unchanged. The solver should update control

// This only does multiframe target
int cpu_vertical_line_cgsolve_cpp( float* l_float, float* target, float* reg, float* control, float* u, int nl, int ti, int nt, int nIter, float zCompress, int useZCompress, float alpha, float max_grad, int init_control) {
    int density_corr = 1;
	assert(nIter >= 0); // Must be some number of iterations
	assert(alpha >= 0); // 
	assert(alpha <= 1); //
    assert(ti < nt);
	int iter, hi, tj;
	assert(nl < CG_MAX_ARRAY_LENGTH);
	assert(nl*nt < CG_MAX_ARRAY_LENGTH);
    float new_residual_norm, old_residual_norm, direction_norm, lastTotalEnergy, totalEnergy;
    lastTotalEnergy=0;
    totalEnergy=0;
    double stepSize=0, residual_ratio=0;
	float residual[CG_MAX_ARRAY_LENGTH];
	float gradient[CG_MAX_ARRAY_LENGTH];
	float conj_gradient[CG_MAX_ARRAY_LENGTH];
	float direction[CG_MAX_ARRAY_LENGTH];
	float l_inds[CG_MAX_ARRAY_LENGTH];
    float* l_control = control + ti*nl;
    float* l_u = u + ti*nl;
    float* l_residual = residual + ti*nl;
    float* l_gradient = gradient + ti*nl;
    float* l_conj_gradient = conj_gradient + ti*nl;
    float* l_direction = direction + ti*nl;
    float* l_target = target + ti*nl;
    float* l_reg = reg + ti*nl;
    
	for ( hi=0; hi< nl; hi++) {
        l_inds[hi] = hi;
    }
	
    // Ax is the gradient due to displacements x
    // Solve for Ax equals zero (i.e. b=0)

    
    // Initialise r
    for ( hi=0; hi< nl; hi++) l_residual[hi] = 0;
    // Intitialise p
	for ( hi=0; hi< nl; hi++) l_direction[hi] = 0;
    // Initialise Ap
    for ( hi=0; hi< nl; hi++) l_conj_gradient[hi] = 0;

    // r=b-Ax
    cpu_vertical_line_compute_gradient(l_float, target, u, nl, ti, nt, zCompress, useZCompress, alpha, gradient, max_grad, 0, density_corr);
    
  //  float totalGradient = 0;
  //  for ( hi=0; hi< nl; hi++) totalGradient += gradient[hi];
    
    // r=-Ax
    for ( hi=0; hi< nl; hi++) l_residual[hi] = -l_gradient[hi];
    // p=r
    if (init_control > 0)
        for ( hi=0; hi< nl; hi++) l_direction[hi] =  -l_gradient[hi];
    else
        for ( hi=0; hi< nl; hi++) l_direction[hi] = l_u[hi] -l_gradient[hi];
 
    // r*r
    old_residual_norm = 0;
    for ( hi=0; hi< nl; hi++) old_residual_norm += l_residual[hi]*l_residual[hi];
    if (old_residual_norm == 0) nIter=0;
   
	for (iter=1; iter <= nIter; iter++) {
        // Compute Ap
        lastTotalEnergy = totalEnergy;
        totalEnergy = cpu_vertical_line_compute_gradient(l_float, target, direction, nl, ti, nt, zCompress, useZCompress, alpha, conj_gradient, max_grad, 0, density_corr);
        
        // p*Ap
        direction_norm = 0;
        for ( hi=0; hi< nl; hi++) direction_norm += l_direction[hi]*l_conj_gradient[hi];
        
        if (fabs(direction_norm) < 1e-20) {
#ifndef NDEBUG
            printf("[DEBUG] Stopping at iteration %i. direction_norm too low\n",iter);
#endif
            break;
        }
        if (fabs(old_residual_norm) < 1e-20) {
#ifndef NDEBUG
            printf("[DEBUG] Stopping at iteration %i. old_residual_norm too low\n",iter);
#endif
            break;
        }
        if (fabs(old_residual_norm) > 1e10) {
#ifndef NDEBUG
            printf("[DEBUG] Stopping at iteration %i. old_residual_norm too high\n",iter);
#endif
            break;
        }
        
        // a = r*r/(p*Ap)
        stepSize = old_residual_norm/direction_norm;
        
        assert(fabs(stepSize) >= 0);

        // x = x + a*p
        for ( hi=0; hi< nl; hi++) {
            l_u[hi] += (float) (stepSize*l_direction[hi]);
        }
        // r = r - a*Ap
        for ( hi=0; hi< nl; hi++) {
            l_residual[hi] -= (float) (stepSize*l_conj_gradient[hi]);
        }

        // r*r
        new_residual_norm = 0;
        for ( hi=0; hi< nl; hi++) new_residual_norm += l_residual[hi]*l_residual[hi];

        // b = r*r / r*r
        residual_ratio = new_residual_norm/old_residual_norm;
        assert(fabs(residual_ratio) >= 0);
        
        // p = r + b*p
        for ( hi=0; hi< nl; hi++) {
            l_direction[hi] = (float) (l_residual[hi] + residual_ratio*l_direction[hi]);
        }
  
#ifndef NDEBUG
        if (iter % 100 == 1)
            printf("[DEBUG] Iteration: %i, Step size: %f, residual_norm: %f, direction_norm: %f, residual_ratio: %f, Energy %f\n", iter, stepSize, old_residual_norm, direction_norm,residual_ratio, totalEnergy);
#endif
  
        old_residual_norm = new_residual_norm;
  
	}

    float l_control_tmp[CG_MAX_ARRAY_LENGTH];
    for (hi=0 ; hi < nl; hi++) {
        if ((hi + l_u[hi]) < 0) l_u[hi]=-hi;
        if ((hi + l_u[hi]) > (nl-1)) l_u[hi]=(nl-1)-hi;
        assert(l_control[hi] >= 0);
        assert(l_control[hi] < nl);
        l_control_tmp[hi] = l_control[hi];
        l_control[hi] = hi + l_u[hi];
        assert(l_control[hi] >= 0);
        assert(l_control[hi] < nl);
    }

    cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg);

    
    // Mass preserving image registration.
    float zdens, zgradient;
    for ( hi=0; hi< nl; hi++) {
        zdens = 1.0;
        if ((hi > 0) & (hi < (nl-1))) {
            //            zgradient = (u[hi+1] + (h+1) - (u[hi-1] + (h-1)))/2.0;
            // Compute the first derivative of the line transform hi => hi + u[hi]
            //            zgradient = (u[hi+1] + (h+1) - (u[hi-1] + (h-1)))/2.0;
            zgradient = (2 + l_u[hi+1] - l_u[hi-1])/2.0;
            if (density_corr > 0) zdens = fabs(zgradient);
        }
        l_reg[hi] = l_reg[hi]*zdens;
        assert(l_inds[hi] == hi);
    }
     

    for (hi=0 ; hi < nl; hi++) {
        l_u[hi] = l_control_tmp[hi] - hi; // Overwrite such that u is unchanged, but control points are
	}
    
    return iter; // Return the number of iterations used
}

