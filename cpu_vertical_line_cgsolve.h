
void cpu_vertical_line_warp( float* t_float, float* t_u, float* t_control, float* t_reg, int nl, int ti, int nt, int multiframe_float, int density_corr);

float cpu_vertical_line_compute_gradient( float* l_float, float* l_target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float* gradient,float max_grad, int multiframe_float,int density_corr);

float cpu_vertical_line_compute_energy( float* l_float, float* target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float max_grad, int multiframe_float, int density_corr);

/*
float cpu_vertical_line_compute_variance_energy( float* l_float, float* target, float* u, int nl, int ti, int nt, float zCompress, int useZCompress, float alpha, float max_grad, int multiframe_float, int density_corr);
*/

float cpu_vertical_line_compute_average( float* t_float, float* l_average, float* u, int nl, int nt, int density_corr );


int cpu_vertical_line_cgsolve_cpp( float* l_float, float* l_target, float* l_reg, float* l_control, float* u, int nl, int ti, int nt, int nIter, float zCompress, int useZCompress, float alpha, float max_grad, int  init_control);




