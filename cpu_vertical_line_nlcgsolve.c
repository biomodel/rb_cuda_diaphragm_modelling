

#include "cpu_vertical_line.h"
#include "cpu_vertical_line_cgsolve.h"
#include "cpu_vertical_line_nlcgsolve.h"
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <omp.h>

const int NLCG_MAX_ARRAY_LENGTH = 100*512;

// http://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf
// Warp a floating curve into a target curve by minimising absolute difference in combination with 1D wave equation along the curve.
// stepSize is the step length
// nIter is the number of iterations
// l_float[nl]
// target[nl*nt]
// reg[nl*nt]
// control[nl*nt]
// u[nl*nt]
// u should remain unchanged. The solver should update control
int cpu_vertical_line_nlcgsolve_cpp( float* t_float, float* t_target, float* t_reg, float* t_control, float* t_u, int nl, int ti, int nt, int nIter, float zCompress, int useZCompress, float alpha, float max_grad, int init_control, int multiframe_float, int density_corr) {
	assert(nIter >= 0); // Must be some number of iterations
	assert(alpha >= 0); // 
	assert(alpha <= 1); //
    assert(ti < nt);
	int iter, hi, tj;
	assert(nl < NLCG_MAX_ARRAY_LENGTH);
	assert(nl*nt < NLCG_MAX_ARRAY_LENGTH);
    float new_residual_norm, old_residual_norm, mid_residual_norm, init_residual_norm, direction_norm, totalEnergy;
    totalEnergy=0;
    double residual_ratio=0;
	float t_u_best[NLCG_MAX_ARRAY_LENGTH];
	float t_u_orig[NLCG_MAX_ARRAY_LENGTH];
	float gradient[NLCG_MAX_ARRAY_LENGTH];
	float gradient_prev[NLCG_MAX_ARRAY_LENGTH];
	float direction[NLCG_MAX_ARRAY_LENGTH];
	float l_inds[NLCG_MAX_ARRAY_LENGTH];
 //   float* l_control = t_control + ti*nl;
    float* l_u_best = t_u_best + ti*nl;
    float* l_u_orig = t_u_orig + ti*nl;
    float* l_u = t_u + ti*nl;
    float* l_gradient = gradient + ti*nl;
    float* l_gradient_prev = gradient_prev + ti*nl;
    float* l_direction = direction + ti*nl;
    float* l_target = t_target + ti*nl;
    float* l_reg = t_reg + ti*nl;
    
    float maxLineIter = 100;
    int lineIter=0;
    float epsilon = 1e-5;
    
	for ( hi=0; hi< nl; hi++) {
        l_inds[hi] = hi;
    }
	
    // calculate f'(x)
    totalEnergy = cpu_vertical_line_compute_gradient(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, gradient, max_grad, multiframe_float, density_corr);
    assert(isfinite(totalEnergy));
    
    // Check that gradient and energy functions return the same result
    float testEnergy = cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
    assert(isfinite(testEnergy));

#ifndef NDEBUG
    if (! (fabs(totalEnergy-testEnergy) < epsilon)) {
        printf("[DEBUG] Test energy mismatch. cpu_vertical_line_compute_gradient: %f, cpu_vertical_line_compute_energy: %f\n",totalEnergy,testEnergy);

        
        // Re check that gradient and energy functions return the same result
        totalEnergy = cpu_vertical_line_compute_gradient(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, gradient, max_grad, multiframe_float, density_corr);
        testEnergy = cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
        assert(isfinite(testEnergy));

        printf("[DEBUG] Test energy mismatch. cpu_vertical_line_compute_gradient: %f, cpu_vertical_line_compute_energy: %f\n",totalEnergy,testEnergy);
    }
#endif
 
    assert(fabs(totalEnergy-testEnergy) < epsilon);
    
    //  float totalGradient = 0;
  //  for ( hi=0; hi< nl; hi++) totalGradient += gradient[hi];
    
    // r=-f'(x)
    for ( hi=0; hi< nl; hi++) l_gradient[hi] = -l_gradient[hi];
    // s=r
    for ( hi=0; hi< nl; hi++) l_gradient_prev[hi] = l_gradient[hi];
    // d=s
    for ( hi=0; hi< nl; hi++) l_direction[hi] = l_gradient[hi];
    
    // r*d
    new_residual_norm = 0;
    for ( hi=0; hi< nl; hi++) new_residual_norm += l_gradient[hi]*l_direction[hi];
    if (fabs(new_residual_norm) < epsilon*epsilon) nIter=0;
    init_residual_norm = new_residual_norm;
    
	for (iter=0; iter <= nIter; iter++) {

        if (iter > 0) {
            // calculate f'(x)
            totalEnergy = cpu_vertical_line_compute_gradient(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, gradient, max_grad, multiframe_float, density_corr);

            // r = -f'(x)
            for ( hi=0; hi< nl; hi++) l_gradient[hi] = -l_gradient[hi];
            
            // s*s
            old_residual_norm = new_residual_norm;
            
            // r*s
            mid_residual_norm = 0;
            for ( hi=0; hi< nl; hi++) mid_residual_norm += l_gradient[hi]*l_gradient_prev[hi];
            
            // r*r
            new_residual_norm = 0;
            for ( hi=0; hi< nl; hi++) new_residual_norm += l_gradient[hi]*l_gradient[hi];
            
            
            if (new_residual_norm < init_residual_norm*epsilon) {
#ifndef NDEBUG
                printf("[DEBUG] [%i] New residual norm is too small\n",iter);
#endif
                break;
            }
            
            // Polak–Ribière:
            // b = n_new - n_mid / n_old
//            residual_ratio = (new_residual_norm - mid_residual_norm)/old_residual_norm;
            
            //Fletcher–Reeves:
            // b = n_new  / n_old
            residual_ratio = new_residual_norm/old_residual_norm;
            
#ifndef NDEBUG
            if (iter % 100 == 1)
                printf("[DEBUG] [%i], new r: %f, old r: %f, mid r: %f, residual_ratio: %f, Energy %f\n", iter, new_residual_norm, old_residual_norm, mid_residual_norm,residual_ratio, totalEnergy);
#endif

            // d = s + b * d
            for ( hi=0; hi< nl; hi++) {
                l_direction[hi] = (float) (l_gradient[hi] + residual_ratio*l_direction[hi]);
            }

            // s=r 
            for ( hi=0; hi< nl; hi++) l_gradient_prev[hi] = l_gradient[hi];

        }
        
        // d * d
        direction_norm = 0;
        for ( hi=0; hi< nl; hi++) direction_norm += l_direction[hi]*l_direction[hi];
        
#ifndef NDEBUG
        if (iter % 100 == 1)
            printf("[DEBUG] [%i] Beginning line search with direction norm: %f\n", iter, direction_norm);
#endif
        
        float maxStepSize = nl;
        
        if (maxStepSize*maxStepSize*direction_norm > epsilon) {
            lineIter = 0;
            // while j < j_max
            
            float currentSize = maxStepSize;
            
            float maxLength = fabs(epsilon);
            for ( hi=0; hi< nl; hi++) if (l_gradient[hi] > maxLength) maxLength= fabs(l_gradient[hi]);
            
            float currentValue = cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
            float bestValue = currentValue;
            for ( hi=0; hi< nl; hi++) l_u_orig[hi] = l_u[hi];
            for ( hi=0; hi< nl; hi++) l_u_best[hi] = l_u_orig[hi];
            
            while (lineIter < maxLineIter) {
                
                float currentLength = currentSize/maxLength;
                
                assert(isfinite(currentLength));
                
                for ( hi=0; hi< nl; hi++) l_u[hi] = l_u_orig[hi] + currentLength*l_direction[hi];
                
                currentValue = cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
                
                
                /* The current deformation field is kept if it was the best so far */
                if(currentValue < bestValue){
                    bestValue = currentValue;
#ifndef NDEBUG
                    if (iter % 100 == 1)
                        printf("[DEBUG] [%i] Best field so far. Energy: %f. Length: %f. Size: %f\n",iter, currentValue, currentLength, currentSize);
#endif
                    
                    for ( hi=0; hi< nl; hi++) {
#ifndef NDEBUG
                        if (! isfinite(l_u[hi]))
                            printf("[DEBUG] [%i] Best field so far. Energy: %f. Length: %f. Size: %f\n",iter, currentValue, currentLength, currentSize);
#endif

                        assert(isfinite(l_u[hi]));
                        l_u_best[hi] = l_u[hi];
                    }
					currentSize*=1.1f;
					currentSize = (currentSize<maxStepSize)?currentSize:maxStepSize;
				}
				else{
#ifndef NDEBUG
                    if (iter % 100 == 1)
  //                      printf("[DEBUG] [%i] Reducing length. Energy: %f. Length: %f. Size: %f\n",iter, currentValue, currentLength, currentSize);
#endif
					currentSize*=0.5;
  				}
                
                if (currentLength*currentLength*direction_norm < epsilon) {
#ifndef NDEBUG
                    if (iter % 100 == 1)
                        printf("[DEBUG] [%i] Line step size is too small stopping search\n", iter);
#endif
                    break;
                }
                // j++
                lineIter++;
            }
            
            for ( hi=0; hi< nl; hi++) l_u[hi] = l_u_best[hi];
            
        } else {
#ifndef NDEBUG
            if (iter % 100 == 1)
                printf("[DEBUG] [%i] Direction is too small\n", iter);
            break;            
#endif
        }
 
	}

    float l_control_tmp[NLCG_MAX_ARRAY_LENGTH];
    
    float* l_control = t_control + ti*nl;
    for (hi=0 ; hi < nl; hi++)
        l_control_tmp[hi] = l_control[hi];
    
    cpu_vertical_line_warp( t_float, t_u, t_control, t_reg, nl, ti, nt, multiframe_float, density_corr);
    /*
     for (hi=0 ; hi < nl; hi++) {
        if ((hi + l_u[hi]) < 0) l_u[hi]=-hi;
        if ((hi + l_u[hi]) > (nl-1)) l_u[hi]=(nl-1)-hi;
        assert(l_control[hi] >= 0);
        assert(l_control[hi] < nl);
        l_control_tmp[hi] = l_control[hi];
        l_control[hi] = hi + l_u[hi];
        assert(l_control[hi] >= 0);
        assert(l_control[hi] < nl);
    }

    cpu_vertical_line_interpol(t_float, nl, l_inds, l_control, nl, l_reg);

    
    // Mass preserving image registration.
    float zdens, zgradient;
    for ( hi=0; hi< nl; hi++) {
        zdens = 1.0;
        if ((hi > 0) & (hi < (nl-1))) {
            // Compute the first derivative of the line transform hi => hi + t_u[hi]
            //            zgradient = (u[hi+1] + (h+1) - (u[hi-1] + (h-1)))/2.0;
            zgradient = (2 + l_u[hi+1] - l_u[hi-1])/2.0;
            zdens = fabs(zgradient);
        }
        l_reg[hi] = l_reg[hi]*zdens;
        assert(l_inds[hi] == hi);
    }
     
     */
    // This code is only needed when each time point is sovled separately
    for (hi=0 ; hi < nl; hi++) {
        l_u[hi] = l_control_tmp[hi] - hi; // Overwrite such that u is unchanged, but control points are
	}
    
    return iter; // Return the number of iterations used
}

