

#include "cpu_vertical_line.h"
#include "cpu_vertical_line_cgsolve.h"
#include "cpu_vertical_line_nlcgsolve_time.h"
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <omp.h>

// Solve for all time

const int NLCGT_MAX_ARRAY_LENGTH = 100*512;

// http://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf
// Warp a floating curve into a target curve by minimising absolute difference in combination with 1D wave equation along the curve.
// stepSize is the step length
// nIter is the number of iterations
// l_float[nl]
// target[nl*nt]
// reg[nl*nt]
// control[nl*nt]
// u[nl*nt]
// u should remain unchanged. The solver should update control
int cpu_vertical_line_nlcgsolve_time_cpp( float* t_float, float* t_target, float* t_reg, float* t_control, float* t_u, int nl, int nt, int nIter, float zCompress, int useZCompress, float alpha, float max_grad, int init_control, int multiframe_float, int density_corr, int compute_average) {
	assert(nIter >= 0); // Must be some number of iterations
	assert(alpha >= 0); // 
	assert(alpha <= 1); //
	int iter, hi, ti, hti;
    long nlt = nl*nt;
	assert(nl < NLCGT_MAX_ARRAY_LENGTH);
	assert(nl*nt < NLCGT_MAX_ARRAY_LENGTH);
    float new_residual_norm, old_residual_norm, mid_residual_norm, init_residual_norm, direction_norm, totalEnergy;
    totalEnergy=0;
    double residual_ratio=0;
	float t_u_best[NLCGT_MAX_ARRAY_LENGTH];
	float t_u_orig[NLCGT_MAX_ARRAY_LENGTH];
	float gradient[NLCGT_MAX_ARRAY_LENGTH];
	float gradient_prev[NLCGT_MAX_ARRAY_LENGTH];
	float direction[NLCGT_MAX_ARRAY_LENGTH];
	float l_inds[NLCGT_MAX_ARRAY_LENGTH];
    float maxLineIter = 100;
    int lineIter=0;
    float epsilon = 1e-5;
    
	for ( hi=0; hi< nl; hi++) {
        l_inds[hi] = hi;
    }
	
    if (compute_average > 0) {
        assert(multiframe_float > 0);
        // Overwrite the target with the average image
        float av1 = cpu_vertical_line_compute_average( t_float, t_target, t_u, nl, nt, density_corr ) ;
        float av2 = cpu_vertical_line_compute_average( t_float, t_target, t_u, nl, nt, density_corr ) ;
        
        #ifndef NDEBUG
                if (! (fabs(av1-av2) < epsilon))
                    printf("[DEBUG] Average counts mismatch. First %f and Second %f\n",av1, av2);
        #endif
        assert(fabs(av1-av2) < epsilon);
    }
    
    // calculate f'(x)
    totalEnergy = 0;
    for (ti=0; ti < nt; ti++)
        totalEnergy += cpu_vertical_line_compute_gradient(t_float, t_target,t_u, nl, ti, nt, zCompress, useZCompress, alpha, gradient, max_grad, multiframe_float, density_corr);
    assert(isfinite(totalEnergy));
    
    // Check that gradient and energy functions return the same result
    float testEnergy = 0;
    for (ti=0; ti < nt; ti++)
        testEnergy += cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
    assert(isfinite(testEnergy));
    
#ifndef NDEBUG
    if (! (fabs(totalEnergy-testEnergy) < epsilon))
        printf("[DEBUG] Test energy mismatch. cpu_vertical_line_compute_gradient: %f, cpu_vertical_line_compute_energy: %f\n",totalEnergy,testEnergy);
#endif
    
    assert(fabs(totalEnergy-testEnergy) < epsilon);
 
    for ( hti=0; hti< nlt; hti++) {
        // r=-f'(x)
        gradient[hti] = -gradient[hti];
        // s=r
        gradient_prev[hti] = gradient[hti];
        // d=s
        direction[hti] = gradient[hti];
    }

    // r*d
    new_residual_norm = 0;
    for ( hti=0; hti< nlt; hti++) new_residual_norm += gradient[hti]*direction[hti];
    if (fabs(new_residual_norm) < epsilon*epsilon) nIter=0;
    init_residual_norm = new_residual_norm;
    
	for (iter=0; iter <= nIter; iter++) {

        if (iter > 0) {
            // calculate f'(x)
            totalEnergy = 0;

            if (compute_average > 0) cpu_vertical_line_compute_average( t_float, t_target, t_u, nl, nt, density_corr );

            for (ti=0; ti < nt; ti++)
                totalEnergy += cpu_vertical_line_compute_gradient(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, gradient, max_grad, multiframe_float, density_corr);

            // r = -f'(x)
            for ( hti=0; hti< nlt; hti++) gradient[hti] = -gradient[hti];
            
            // s*s
            old_residual_norm = new_residual_norm;
            
            // r*s
            mid_residual_norm = 0;
            for ( hti=0; hti< nlt; hti++) mid_residual_norm += gradient[hti]*gradient_prev[hti];
            
            // r*r
            new_residual_norm = 0;
            for ( hti=0; hti< nlt; hti++) new_residual_norm += gradient[hti]*gradient[hti];
            
            
            if (new_residual_norm < init_residual_norm*epsilon) {
#ifndef NDEBUG
                printf("[DEBUG] [%i] New residual norm is too small\n",iter);
#endif
                break;
            }
            
            // Polak–Ribière:
            // b = n_new - n_mid / n_old
//            residual_ratio = (new_residual_norm - mid_residual_norm)/old_residual_norm;
            
            //Fletcher–Reeves:
            // b = n_new  / n_old
            residual_ratio = new_residual_norm/old_residual_norm;
            
#ifndef NDEBUG
            if (iter % 100 == 1)
                printf("[DEBUG] [%i] new r: %f, old r: %f, mid r: %f, residual_ratio: %f, Energy %f\n", iter, new_residual_norm, old_residual_norm, mid_residual_norm,residual_ratio, totalEnergy);
#endif

            // d = s + b * d
            for ( hti=0; hti< nlt; hti++) {
                direction[hti] = (float) (gradient[hti] + residual_ratio*direction[hti]);
                assert(isfinite(direction[hti]));
            }

            // s=r 
            for ( hti=0; hti< nlt; hti++) gradient_prev[hti] = gradient[hti];

        }
        
        // d * d
        direction_norm = epsilon;
        for ( hti=0; hti< nlt; hti++) direction_norm += direction[hti]*direction[hti];
        
#ifndef NDEBUG
        if (iter % 100 == 1)
            printf("[DEBUG] [%i] Beginning line search with direction norm: %f\n", iter, direction_norm);
#endif
        
        float maxStepSize = nl;
        
        if (maxStepSize*maxStepSize*direction_norm > epsilon) {
            lineIter = 0;
            // while j < j_max
            
            float currentSize = maxStepSize;
            
            float maxLength = fabs(epsilon);
            for ( hti=0; hti< nlt; hti++) if (fabs(gradient[hti]) > maxLength) maxLength=fabs(gradient[hti]);
            assert(maxLength > 0);
            
            float currentValue = 0;

            if (compute_average > 0) {
                cpu_vertical_line_compute_average( t_float, t_target, t_u, nl, nt, density_corr );
                for (ti=0; ti < nt; ti++)
                    currentValue += cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
            } else {
                for (ti=0; ti < nt; ti++)
                    currentValue += cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
            }
            
            float bestValue = currentValue;
            for ( hti=0; hti< nlt; hti++) t_u_orig[hti] = t_u[hti];
            for ( hti=0; hti< nlt; hti++) t_u_best[hti] = t_u_orig[hti];
            
            while (lineIter < maxLineIter) {
                
                float currentLength = currentSize/maxLength;
                assert(isfinite(currentLength));
                
                for ( hti=0; hti< nlt; hti++) {
                    t_u[hti] = t_u_orig[hti] + currentLength*direction[hti];
                }
                
                currentValue = 0;
                if (compute_average > 0) {
                    cpu_vertical_line_compute_average( t_float, t_target, t_u, nl, nt, density_corr );
                    for (ti=0; ti < nt; ti++)
                        currentValue += cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
                } else {
                    for (ti=0; ti < nt; ti++)
                        currentValue += cpu_vertical_line_compute_energy(t_float, t_target, t_u, nl, ti, nt, zCompress, useZCompress, alpha, max_grad, multiframe_float, density_corr);
                }
                
                /* The current deformation field is kept if it was the best so far */
                if(currentValue < bestValue){
                    bestValue = currentValue;
#ifndef NDEBUG
                    if (iter % 100 == 1)
                        printf("[DEBUG] [%i] Best field so far. Energy: %f. Length: %f. Size: %f\n", iter, currentValue, currentLength, currentSize);
#endif
                    
                    for ( hti=0; hti< nlt; hti++) t_u_best[hti] = t_u[hti];
                    
					currentSize*=1.1f;
					currentSize = (currentSize<maxStepSize)?currentSize:maxStepSize;
				}
				else{
#ifndef NDEBUG
                    if (iter % 100 == 1)
                        printf("[DEBUG] [%i] Reducing length. Energy: %f. Length: %f. Size: %f\n",iter, currentValue, currentLength, currentSize);
#endif
					currentSize*=0.5;
  				}
                
                if (currentLength*currentLength*direction_norm < epsilon) {
#ifndef NDEBUG
                    if (iter % 100 == 1)
                            printf("[DEBUG] [%i] Line step size is too small stopping search\n", iter);
#endif
                    break;
                }
                // j++
                lineIter++;
            }
            
            for ( hti=0; hti< nlt; hti++) t_u[hti] = t_u_best[hti];
            
        } else {
#ifndef NDEBUG
            if (iter % 100 == 1)
                    printf("[DEBUG] [%i] Direction is too small\n", iter);
#endif
            break;
        }
 
	}

    float l_control_tmp[NLCGT_MAX_ARRAY_LENGTH];
    for (ti=0 ; ti < nt; ti++) {

        float* l_control = t_control + ti*nl;
        for (hi=0 ; hi < nl; hi++)
            l_control_tmp[hi] = l_control[hi];

        cpu_vertical_line_warp( t_float, t_u, t_control, t_reg, nl, ti, nt, multiframe_float, density_corr);
/*
        float* l_control = t_control + ti*nl;
        float* l_control_tmp = t_control_tmp + ti*nl;
        float* l_u = u + ti*nl;
        float* l_reg = t_reg + ti*nl;
        
        for (hi=0 ; hi < nl; hi++) {
            assert(isfinite(l_u[hi]));
            if ((hi + l_u[hi]) < 0) l_u[hi]=-hi;
            if ((hi + l_u[hi]) > (nl-1)) l_u[hi]=(nl-1)-hi;
            assert(l_control[hi] >= 0);
            assert(l_control[hi] < nl);
            l_control_tmp[hi] = l_control[hi];
            l_control[hi] = hi + l_u[hi];
            assert(l_control[hi] >= 0);
            assert(l_control[hi] < nl);
        }
        
        cpu_vertical_line_interpol(t_float, nl, l_inds, l_control, nl, l_reg);
        
        
        // Mass preserving image registration.
        float zdens, zgradient;
        for ( hi=0; hi< nl; hi++) {
            zdens = 1.0;
            if ((hi > 0) & (hi < (nl-1))) {
                // Compute the first derivative of the line transform hi => hi + u[hi]
                //            zgradient = (u[hi+1] + (h+1) - (u[hi-1] + (h-1)))/2.0;
                zgradient = (2 + l_u[hi+1] - l_u[hi-1])/2.0;
                zdens = fabs(zgradient);
            }
            l_reg[hi] = l_reg[hi]*zdens;
            assert(l_inds[hi] == hi);
        }
        
        
 */
        float* l_u = t_u + ti*nl;
        for (hi=0 ; hi < nl; hi++) {
            l_u[hi] = l_control_tmp[hi] - hi; // Overwrite such that u is unchanged, but control points are
        }
 }

    return iter; // Return the number of iterations used
}

