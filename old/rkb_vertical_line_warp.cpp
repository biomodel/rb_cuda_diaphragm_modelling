
/*
 * IDL wrapper for calling the vertical line warping C function.
 * Mainly used for testing purposes.
 */

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <idl_export.h>
extern "C" {
	#include "cpu_vertical_line_warp.h"
}
//#include <cuda.h>



float* rkb_ensure_float_array(IDL_VPTR arg) {
	IDL_ENSURE_ARRAY(arg);
	assert(arg->type==IDL_TYP_FLOAT);
	return (float*) arg->value.arr->data; 
}

extern "C" IDL_VPTR rkb_vertical_line_warp(int argc, IDL_VPTR *argv, char *argk) {
	float* l_float = rkb_ensure_float_array(argv[0]);
	int nl = argv[0] -> value.arr->n_elts;
	float* l_target = rkb_ensure_float_array(argv[1]);
	assert(argv[1]->value.arr->n_elts==nl);
	int nIter=100;
	float scale0=0.1;
	int smooth_width=1;
	if (argc > 2) {
		IDL_ENSURE_SCALAR(argv[2]);
		nIter = (int) IDL_LongScalar(argv[2]);
	}
	if (argc > 3) {
		IDL_ENSURE_SCALAR(argv[3]);
		scale0 = (float) IDL_DoubleScalar(argv[3]);
	}
	if (argc > 4) {
		IDL_ENSURE_SCALAR(argv[4]);
		smooth_width = (int) IDL_LongScalar(argv[4]);
	}
	IDL_VPTR ivReturn;
	IDL_MEMINT diml[IDL_MAX_ARRAY_DIM];
	diml[0] = nl;
	float* l_reg = (float*) IDL_MakeTempArray(IDL_TYP_FLOAT, 1, diml, IDL_ARR_INI_NOP, &ivReturn);
	for (int i=0; i< nl; i++) l_reg[i] = 0;
	cpu_vertical_line_warp( l_float, l_target, l_reg, nl, nIter, scale0, smooth_width);
	return ivReturn;
}


extern "C" int IDL_Load(void)
{
	/*
	 * These tables contain information on the functions and procedures
	 * that make up the TESTMODULE DLM. The information contained in these
	 * tables must be identical to that contained in testmodule.dlm.
	 */
	static IDL_SYSFUN_DEF2 procedure_addr[] = {
	};
	
	static IDL_SYSFUN_DEF2 function_addr[] = {
		{ (IDL_SYSRTN_GENERIC) rkb_vertical_line_warp, "RKB_VERTICAL_LINE_WARP", 2, 5, 0, 0},
	};
	
	/*
	 * Register our routine. The routines must be specified exactly the same
	 * as in testmodule.dlm.
	 */
	return IDL_SysRtnAdd(procedure_addr, FALSE, IDL_CARRAY_ELTS(procedure_addr)) && IDL_SysRtnAdd(function_addr, TRUE, IDL_CARRAY_ELTS(function_addr));
}

// g++ -shared -fPIC -DUSE_IDL -I/Applications/itt/idl/idl80/external/include vertical_line_warp.cpp -o vertical_line_warp.so

// g++ -shared -fPIC -DUSE_IDL -I/usr/local/itt/idl/idl80/external/include vertical_line_warp.cpp -o vertical_line_warp.so
// LD_LIBRARY_PATH=/usr/local/itt/idl/idl80/bin/bin.linux.x86_64 gdb /usr/local/itt/idl/idl80/bin/bin.linux.x86_64/idl

