
const int MAX_ARRAY_LENGTH = 512;


int cpu_vertical_line_warp( float* l_float, float* l_target, float* l_reg, int nl, int nIter, float scale0, int smooth_width);
int cpu_vertical_line_warp_control( float* l_float, float* l_target, float* l_reg, float *l_control, int nl, int nIter, float scale0, int smooth_width);


