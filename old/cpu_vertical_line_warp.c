
#include "cpu_vertical_line_warp.h"
#include "cpu_vertical_line.h"
#include <math.h>
#include <assert.h>


int cpu_vertical_line_warp( float* l_float, float* l_target, float* l_reg, int nl, int nIter, float scale0, int smooth_width) {
	assert(nl < MAX_ARRAY_LENGTH);	
	float l_control[MAX_ARRAY_LENGTH];
	int j;
	for ( j=0; j< nl; j++) l_control[j] = 0;
	return cpu_vertical_line_warp_control( l_float, l_target, l_reg, l_control, nl, nIter, scale0, smooth_width);
}
// Warp a floating curve into a target curve by solving the finite differences for a 1D wave equation along the curve.
// scale0 is the step length (as a fraction of total restoring force necessary)
// nIter is the number of iterations
// Optionally apply a smoothing kernel to the displacement field after each iteration.
int cpu_vertical_line_warp_control( float* l_float, float* l_target, float* l_reg, float* l_control, int nl, int nIter, float scale0, int smooth_width) {
	assert(nl < MAX_ARRAY_LENGTH);	
	assert(nIter >= 0); // Must be some number of iterations
	assert(scale0 > 0); // A negative scale factor applys force in wrong direction
	assert(scale0 <= 1); // Scaling the force above the actual finite difference would over correct.
	assert(smooth_width > 0); // The smoothing width must be more than 1. 1=No smoothing
	int iter, j;
	float u[MAX_ARRAY_LENGTH];
	for ( j=0; j< nl; j++) u[j] = 0;
	float l_inds[MAX_ARRAY_LENGTH];
	for ( j=0; j< nl; j++) l_inds[j] = j;
	float u_min[MAX_ARRAY_LENGTH];
	for ( j=0; j< nl; j++) u_min[j] = 0;
	float l_sum[MAX_ARRAY_LENGTH];// No need for initialisation. Initialised and used inside loop,
	float l_diff[MAX_ARRAY_LENGTH];// No need for initialisation. Initialised and used inside loop,
	float l_smoothed_diff[MAX_ARRAY_LENGTH];// No need for initialisation. Initialised and used inside loop,
	float l_cost = cpu_vertical_line_cost(l_target,l_float,nl);
	float l_cost_min = l_cost;
	for (iter=1; iter <= nIter; iter++) {
		cpu_vertical_line_math_add(l_inds,u,l_control,nl);
		cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg);
		l_cost = cpu_vertical_line_cost(l_target,l_float,nl);
		for (j=0; j < nl; j++) l_diff[j] = l_target[j] - l_reg[j];
		cpu_vertical_line_smooth(l_diff,smooth_width,nl, l_smoothed_diff);
		l_sum[0] = l_smoothed_diff[0]; 
		for (j=1; j < nl; j++) l_sum[j] = l_sum[j-1] + l_smoothed_diff[j]; 
		for (j=1; j < nl; j++) l_sum[j] = l_sum[j-1] + l_sum[j]; // Double integral
		if ( l_sum[nl-1] > 0) {
//			cpu_vertical_line_smooth(l_sum,smooth_width,nl, l_smoothed_sum);
			for ( j=0; j < nl; j++) u[j] += scale0*l_sum[j]/l_sum[nl-1];
			if (l_cost < l_cost_min*100) {
				for ( j=0; j < nl; j++) u_min[j] = u[j];
				l_cost_min = l_cost;			
				//		std::cout << " Accepted warping for iteration " << iter << "\n";
			}
		} else {
//					printf("Total difference is zero. Cannot iterate any further\n");
			break;
		}
	}
	cpu_vertical_line_math_add(l_inds,u_min,l_control,nl);
	cpu_vertical_line_interpol(l_float, nl, l_inds, l_control, nl, l_reg);
	for ( j=0; j< nl; j++) assert(l_inds[j] == j);
	return iter; // Return the number of iterations used
}

