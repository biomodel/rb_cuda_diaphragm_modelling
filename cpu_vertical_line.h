


float cpu_vertical_line_cost( float* l_float, float* l_target, int nl);
void cpu_vertical_line_math_add( float* l_a, float* l_b, float* l_r, int nl);
void cpu_vertical_line_smooth( float* array, int width, int nl, float* result);
void cpu_vertical_line_interpol( float* v, int nv, float* x, float* u, int nu, float* vout);
void cpu_vertical_line_deriv( float* x, int nx, float* yout);
void cpu_vertical_line_deriv5( float* x, int nx, float* yout);




