

#include "cpu_vertical_line.h"
#include <math.h>
#include <assert.h>



float cpu_vertical_line_cost( float* l_float, float* l_target, int nl) {
	float total_cost = 0;
	int i;
	for (i=0; i<nl; i++) total_cost += fabs(l_float[i] - l_target[i]);
	return total_cost;
}

void cpu_vertical_line_math_add( float* l_a, float* l_b, float* l_r, int nl) {
	int i;
	for (i=0; i<nl; i++) l_r[i] = l_a[i] + l_b[i];
}

// Smooth the 1d array using a kernel of a given width. 
void cpu_vertical_line_smooth( float* array, int width, int nl, float* result) {
	//	std::cout << "Width: " << width << "\n";
	//	std::cout << "Number of elements: " << nl << "\n";
	assert(width > 0);
	assert(nl > 0);
	assert(width*2 < nl);
	int i, j, k;
	if (width==1) {
		for (i=0; i < nl; i++) result[i] = array[i];
		//		std::cout << "No smoothing\n";
		return;
	}
	// Iterate over each position in the array
	for (i=0; i < nl; i++) {
		// Iterate over each position in the kernel
		for (j=0; j < width; j++) {
			k = i + j - width/2;
			// Truncate the edges
			if (k < 0) k = 0;
			if (k >= nl-1) k = nl-1;
			result[i] += array[k];
		}
		// Normalise the result
		result[i] = result[i]/width;
	}
}

// Performs linear interpolation on vectors with an irregular grid.
// v[nv] An input vector of any type except string.
// x[nv] The abscissa values for V, in the irregularly-gridded case. X must have the same number of elements as V, and the values must be strictly ascending.
// u[nu] The abscissa values for the result. The result will have the same number of elements as u. u does not need to be monotonic.
// vout[nu] The interpolated result
void cpu_vertical_line_interpol( float* v, int nv, float* x, float* u, int nu, float* vout) {
	assert(nv > 1);
	assert(nu > 1);
	int xi = 0;
	int xlocate = 0;
	// Iterate over all abscissa values for the result
	int ui;
	for (ui=0; ui < nu; ui++) {
		xlocate = xi;
		// Find the next abscissa value in x which is greater than this abscissa value
		while ((x[xlocate] < u[ui]) & (xlocate < nv)) xlocate++;
		xlocate--; // Step back once
		if (xlocate > nv-2) xlocate = nv-2;
		if (xlocate < xi) xlocate = xi;
		xi = xlocate;
		assert(x[xi] < x[xi+1]);
        assert(xi >= 0);
        assert((xi+1) < nv);
		//		assert(v[xi] < v[xi+1]);
		vout[ui] = (u[ui] - x[xi])*(v[xi+1]-v[xi])/(x[xi+1]-x[xi]) + v[xi];
	}
}

//	Perform numerical differentiation using 3-point, Lagrangian 
// interpolation.
void cpu_vertical_line_deriv( float* x, int nx, float* yout) {
	assert(nx > 2);
	int xi=0;	
	for (xi=1; xi < (nx-1); xi++) yout[xi] = (x[xi+1] - x[xi-1])/2.;
	yout[0] = (-3.0*x[0] + 4.0*x[1] - x[2])/2.;
    yout[nx-1] = (3.*x[nx-1] - 4.*x[nx-2] + x[nx-3])/2.;
}


//	Perform numerical differentiation using 5-point stencil.
void cpu_vertical_line_deriv5( float* x, int nx, float* yout) {
	assert(nx > 2);
	int xi=0;
	for (xi=2; xi < (nx-2); xi++)
        yout[xi] = (-x[xi+2] + 8*x[xi+1] - 8*x[xi-1] + x[xi-2])/12.0;
	yout[nx-2] = (x[nx-1] - x[nx-3])/2.;
    yout[nx-1] = (3.*x[nx-1] - 4.*x[nx-2] + x[nx-3])/2.;
	yout[1] = (x[2] - x[0])/2.;
	yout[0] = (-3.0*x[0] + 4.0*x[1] - x[2])/2.;
}
